﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab3.Contract;

namespace Lab3.Implementation
{
    public class Winda : IZasilanie, IPanelSterowania, IWyswietlacz
    {
        void IPanelSterowania.SterujWinda()
        {
            Console.WriteLine("Winda w ruchu");
        }

        string IPanelSterowania.PobierzStatusWindy()
        {
            return "Pobrano status windy";
        }

        void IZasilanie.WlaczZasilanie()
        {
            Console.WriteLine("Włączam zasilanie...");
        }

        void IZasilanie.OdlaczZasilanie()
        {
            Console.WriteLine("Odłączam zasilanie...");
        }

        void IWyswietlacz.WyswietlInformacje()
        {
            Console.WriteLine("Wyświetlam informacje");
        }

        void IWyswietlacz.Wyczysc()
        {
            Console.WriteLine();
        }

        public static object StworzZasilacz(object obj)
        {
            if (obj is IZasilanie)
            {
                return obj as IZasilanie;
            }

            return null;
        }

        public static object StworzPanelSterowania(object obj)
        {
            if (obj is IPanelSterowania)
            {
                return obj as IPanelSterowania;
            }

            return null;
        }

        public static object StworzWyswietlacz(object obj)
        {
            if (obj is IZasilanie)
            {
                return obj as IZasilanie;
            }

            return null;
        }
    }
}
