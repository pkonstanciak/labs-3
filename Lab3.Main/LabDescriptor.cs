﻿using System;
using Lab3.Contract;
using Lab3.Implementation;

namespace Lab3
{
    public struct LabDescriptor
    {
        #region definitions

        public delegate object GetInstance(object component);
        
        #endregion

        #region P1

        public static Type I1 = typeof(IZasilanie);
        public static Type I2 = typeof(IPanelSterowania);
        public static Type I3 = typeof(IWyswietlacz);

        public static Type Component = typeof(Winda);

        public static GetInstance GetInstanceOfI1 = Winda.StworzZasilacz;
        public static GetInstance GetInstanceOfI2 = Winda.StworzPanelSterowania;
        public static GetInstance GetInstanceOfI3 = Winda.StworzWyswietlacz;
        
        #endregion

        #region P2

        public static Type Mixin = typeof(KosmiczneRadio);
        public static Type MixinFor = typeof(Winda);

        #endregion
    }
}
